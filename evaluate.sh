set -e

python3 -m ehealthkd.evaltest --gold corpus-master/ --mode dev --pretty --submit submissions/ > dev.json
python3 -m ehealthkd.evaltest --gold corpus-master/ --mode test --pretty --submit submissions/ > test.json
