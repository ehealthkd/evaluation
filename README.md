# Evaluation Environment for eHealth-KD

This repository serves as a continuous evaluation scenario for the [eHealth-KD corpus](https://ehealthkd.github.io).

To submit your results for evaluation and appear in the leaderboard, follow the instructions in
the [eHealth-KD website](https://ehealthkd.gitlab.io/evaluation).

**NOTE**: Participating in the evaluation inmediately makes **your submission data** licensed CC By-NC-SA 4.0) and publicly available under those terms.

## License

Copyright 2020 - The eHealth Knowledge Discovery <https://ehealthkd.gitlab.io> contributors.

All content in this repository is licensed under CC By-NC-SA 4.0.

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Dataset" property="dct:title" rel="dct:type">eHealth Knowledge Discovery</span> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
